# MC Unified Bookshelf
A library for reading and writing a variety of minecraft files.

## License
licensed under: https://www.gnu.org/licenses/gpl-3.0.html
or any later version at your option.