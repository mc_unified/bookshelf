use serde::{
    de::{Error, VariantAccess, Visitor, EnumAccess, IgnoredAny},
    Deserialize, Deserializer, Serialize, Serializer
};
use std::{
    collections::HashMap,
    fmt::{self, Display, Formatter},
    marker::PhantomData
};

use super::TagType;

/// Represents a NBT Compound,
/// implements [`serde::Serialize`] and [`serde::Deserialize`]
pub type Compound = HashMap<String, Tag>;

/// Represents any NBT Tag,
/// implements [`serde::Serialize`] and [`serde::Deserialize`]
#[derive(Clone, PartialEq, Debug)]
pub enum Tag {
    ///A 8-bit signed integer
    Byte(i8),
    ///A 16-bit signed integer
    Short(i16),
    ///A 32-bit signed integer
    Int(i32),
    ///A 64-bit signed integer
    Long(i64),
    ///A 32-bit floating point encoded as "binary32" as defined in IEEE 754-2008
    Float(f32),
    ///A 64-bit floating point encoded as "binary64" as defined in IEEE 754-2008
    Double(f64),
    ///An array of 8-bit signed integers
    ByteArray(Vec<i8>),
    ///A String
    String(String),
    ///A list of nameless tags of the same type
    List(List),
    ///A list of named tags of different types
    Compound(Compound),
    ///An array of 32-bit signed integers
    IntArray(Vec<i32>),
    ///An array of 64-bit signed integers
    LongArray(Vec<i64>),
}

impl Serialize for Tag {
    fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        match self {
            Tag::Byte(v) => v.serialize(serializer),
            Tag::Short(v) => v.serialize(serializer),
            Tag::Int(v) => v.serialize(serializer),
            Tag::Long(v) => v.serialize(serializer),
            Tag::Float(v) => v.serialize(serializer),
            Tag::Double(v) => v.serialize(serializer),
            Tag::ByteArray(v) => byte_array::serialize(v, serializer),
            Tag::String(v) => v.serialize(serializer),
            Tag::List(v) => v.serialize(serializer),
            Tag::Compound(v) => v.serialize(serializer),
            Tag::IntArray(v) => int_array::serialize(v, serializer),
            Tag::LongArray(v) => long_array::serialize(v, serializer),
        }
    }
}

impl<'de> Deserialize<'de> for Tag {
    fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
        deserializer.deserialize_any(TagVisitor)
    }
}

struct TagVisitor;

impl<'de> Visitor<'de> for TagVisitor {
    type Value = Tag;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(formatter, "a NBT value")
    }

    fn visit_i8<E: Error>(self, v: i8) -> Result<Self::Value, E> {
        Ok(Tag::Byte(v))
    }

    fn visit_i16<E: Error>(self, v: i16) -> Result<Self::Value, E> {
        Ok(Tag::Short(v))
    }

    fn visit_i32<E: Error>(self, v: i32) -> Result<Self::Value, E> {
        Ok(Tag::Int(v))
    }

    fn visit_i64<E: Error>(self, v: i64) -> Result<Self::Value, E> {
        Ok(Tag::Long(v))
    }

    fn visit_f32<E: Error>(self, v: f32) -> Result<Self::Value, E> {
        Ok(Tag::Float(v))
    }

    fn visit_f64<E: Error>(self, v: f64) -> Result<Self::Value, E> {
        Ok(Tag::Double(v))
    }

    fn visit_str<E: Error>(self, v: &str) -> Result<Self::Value, E> {
        Ok(Tag::String(v.to_owned()))
    }

    fn visit_string<E: Error>(self, v: String) -> Result<Self::Value, E> {
        Ok(Tag::String(v))
    }

    fn visit_map<A: serde::de::MapAccess<'de>>(self, map: A) -> Result<Self::Value, A::Error> {
        visit_map(map).map(Tag::Compound)
    }

    fn visit_enum<A: serde::de::EnumAccess<'de>>(self, data: A) -> Result<Self::Value, A::Error> {
        let (array_type, variant) = data.variant()?;
        match array_type {
            ArrayType::Byte => Ok(Tag::ByteArray(variant.newtype_variant()?)),
            ArrayType::Int => Ok(Tag::IntArray(variant.newtype_variant()?)),
            ArrayType::Long => Ok(Tag::LongArray(variant.newtype_variant()?)),
        }
    }

    fn visit_seq<A: serde::de::SeqAccess<'de>>(self, seq: A) -> Result<Self::Value, A::Error> {
        ListVisitor.visit_seq(seq).map(Tag::List)
    }
}

fn visit_map<'de, A: serde::de::MapAccess<'de>>(mut map: A) -> Result<Compound, A::Error> {
    let mut compound = Compound::new();

    while let Some((k, v)) = map.next_entry()? {
        compound.insert(k, v);
    }

    Ok(compound)
}

/// Represents any NBT List,
/// implements [`serde::Serialize`] and [`serde::Deserialize`]
#[derive(Clone, PartialEq, Debug)]
pub enum List {
    ///A generic length zero list, isn't bounded by any type
    Empty,
    ///A list of nameless byte tags
    Byte(Vec<i8>),
    ///A list of nameless short tags
    Short(Vec<i16>),
    ///A list of nameless int tags
    Int(Vec<i32>),
    ///A list of nameless long tags
    Long(Vec<i64>),
    ///A list of nameless float tags
    Float(Vec<f32>),
    ///A list of nameless double tags
    Double(Vec<f64>),
    ///A list of nameless byte array tags
    ByteArray(Vec<Vec<i8>>),
    ///A list of nameless string tags
    String(Vec<String>),
    ///A list of nameless list tags, each list can hold a different type of tag
    List(Vec<List>),
    ///A list of nameless compound tags
    Compound(Vec<Compound>),
    ///A list of nameless int array tags
    IntArray(Vec<Vec<i32>>),
    ///A list of nameless long array tags
    LongArray(Vec<Vec<i64>>),
}

impl Serialize for List {
    fn serialize<S: ::serde::Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        match self {
            List::Empty => [(); 0].serialize(serializer),
            List::Byte(l) => l.serialize(serializer),
            List::Short(l) => l.serialize(serializer),
            List::Int(l) => l.serialize(serializer),
            List::Long(l) => l.serialize(serializer),
            List::Float(l) => l.serialize(serializer),
            List::Double(l) => l.serialize(serializer),
            List::ByteArray(l) => l.serialize(serializer),
            List::String(l) => l.serialize(serializer),
            List::List(l) => l.serialize(serializer),
            List::Compound(l) => l.serialize(serializer),
            List::IntArray(l) => l.serialize(serializer),
            List::LongArray(l) => l.serialize(serializer),
        }
    }
}

struct ListVisitor;

impl<'de> Visitor<'de> for ListVisitor {
    type Value = List;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "an NBT list")
    }

    fn visit_seq<A: serde::de::SeqAccess<'de>>(self, mut seq: A) -> Result<Self::Value, A::Error> {
        let mut list = List::Empty;

        while seq.next_element_seed(ListElement(&mut list))?.is_some() {}

        Ok(list)
    }
}

struct ListElement<'a>(&'a mut List);

impl<'de, 'a> serde::de::DeserializeSeed<'de> for ListElement<'a> {
    type Value = ();

    fn deserialize<D: serde::Deserializer<'de>>(
        self,
        deserializer: D,
    ) -> Result<Self::Value, D::Error> {
        deserializer.deserialize_any(self)
    }
}

macro_rules! visit {
    ($self:expr, $variant:ident, $value:expr, $error:ty) => {
        match $self.0 {
            List::Empty => {
                *$self.0 = List::$variant(vec![$value]);
                Ok(())
            }
            List::$variant(l) => {
                l.push($value);
                Ok(())
            }
            _ => Err(<$error>::custom("NBT lists must be homogenous")),
        }
    };
}

impl<'de, 'a> Visitor<'de> for ListElement<'a> {
    type Value = ();

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "a valid NBT list element")
    }

    fn visit_i8<E: Error>(self, v: i8) -> Result<Self::Value, E> {
        visit!(self, Byte, v, E)
    }

    fn visit_i16<E: Error>(self, v: i16) -> Result<Self::Value, E> {
        visit!(self, Short, v, E)
    }

    fn visit_i32<E: Error>(self, v: i32) -> Result<Self::Value, E> {
        visit!(self, Int, v, E)
    }

    fn visit_i64<E: Error>(self, v: i64) -> Result<Self::Value, E> {
        visit!(self, Long, v, E)
    }

    fn visit_f32<E: Error>(self, v: f32) -> Result<Self::Value, E> {
        visit!(self, Float, v, E)
    }

    fn visit_f64<E: Error>(self, v: f64) -> Result<Self::Value, E> {
        visit!(self, Double, v, E)
    }

    fn visit_str<E: Error>(self, v: &str) -> Result<Self::Value, E> {
        visit!(self, String, v.to_owned(), E)
    }

    fn visit_string<E: Error>(self, v: String) -> Result<Self::Value, E> {
        visit!(self, String, v, E)
    }

    fn visit_seq<A: serde::de::SeqAccess<'de>>(self, seq: A) -> Result<Self::Value, A::Error> {
        visit!(self, List, ListVisitor.visit_seq(seq)?, A::Error)
    }

    fn visit_map<A: serde::de::MapAccess<'de>>(self, map: A) -> Result<Self::Value, A::Error> {
        visit!(self, Compound, visit_map(map)?, A::Error)
    }

    fn visit_enum<A: serde::de::EnumAccess<'de>>(self, data: A) -> Result<Self::Value, A::Error> {
        let (array_type, variant) = data.variant()?;
        match array_type {
            ArrayType::Byte => visit!(self, ByteArray, variant.newtype_variant()?, A::Error),
            ArrayType::Int => visit!(self, IntArray, variant.newtype_variant()?, A::Error),
            ArrayType::Long => visit!(self, LongArray, variant.newtype_variant()?, A::Error),
        }
    }
}

// internal names used to deserialize and serialize
// arrays using enum handlers
pub(crate) const BYTE_ARRAY_NAME: &str = "__byte_array__";
pub(crate) const INT_ARRAY_NAME: &str = "__int_array__";
pub(crate) const LONG_ARRAY_NAME: &str = "__long_array__";

pub(crate) const ARRAY_ENUM_NAME: &str = "__array__";
pub(crate) const ARRAY_VARIANTS: &[&str] = &[BYTE_ARRAY_NAME, INT_ARRAY_NAME, LONG_ARRAY_NAME];

macro_rules! array {
    ($mod:ident, $tag:literal, $index:literal, $variant:tt) => {
        pub mod $mod {
            use super::*;

            pub fn serialize<T, S>(array: &T, serializer: S) -> core::result::Result<S::Ok, S::Error>
            where
                T: Serialize,
                S: Serializer,
            {
                serializer.serialize_newtype_variant(ARRAY_ENUM_NAME, $index, $variant, array)
            }

            pub fn deserialize<'de, T, D>(deserializer: D) -> core::result::Result<T, D::Error>
            where
                T: Deserialize<'de>,
                D: Deserializer<'de>,
            {
                struct ArrayVisitor<T>(PhantomData<T>);

                impl<'de, T: Deserialize<'de>> Visitor<'de> for ArrayVisitor<T> {
                    type Value = T;

                    fn expecting(&self, formatter: &mut Formatter) -> std::fmt::Result {
                        write!(formatter, $tag)
                    }

                    fn visit_enum<A>(self, data: A) -> core::result::Result<Self::Value, A::Error>
                    where
                        A: EnumAccess<'de>,
                    {
                        // Ignore the variant name.
                        let (_, variant) = data.variant::<IgnoredAny>()?;

                        variant.newtype_variant()
                    }
                }

                deserializer.deserialize_enum(
                    ARRAY_ENUM_NAME,
                    ARRAY_VARIANTS,
                    ArrayVisitor(PhantomData),
                )
            }
        }
    };
}

array!(byte_array, "Tag_BYTE_ARRAY", 0, BYTE_ARRAY_NAME);
array!(int_array, "Tag_INT_ARRAY", 1, INT_ARRAY_NAME);
array!(long_array, "Tag_LONG_ARRAY", 2, LONG_ARRAY_NAME);

enum ArrayType {
    Byte,
    Int,
    Long,
}

impl<'de> Deserialize<'de> for ArrayType {
    fn deserialize<D: ::serde::Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
        struct ArrayVisitor;

        impl<'de> Visitor<'de> for ArrayVisitor {
            type Value = ArrayType;

            fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
                write!(formatter, "a u8 or string encoding an NBT array type")
            }

            fn visit_u8<E: Error>(self, v: u8) -> Result<Self::Value, E> {
                match v {
                    0 => Ok(ArrayType::Byte),
                    1 => Ok(ArrayType::Int),
                    2 => Ok(ArrayType::Long),
                    i => Err(E::custom(format!("invalid array type index `{i}`"))),
                }
            }

            fn visit_str<E: Error>(self, v: &str) -> Result<Self::Value, E> {
                match v {
                    BYTE_ARRAY_NAME => Ok(ArrayType::Byte),
                    INT_ARRAY_NAME => Ok(ArrayType::Int),
                    LONG_ARRAY_NAME => Ok(ArrayType::Long),
                    s => Err(E::custom(format!("invalid array type `{s}`"))),
                }
            }
        }

        deserializer.deserialize_u8(ArrayVisitor)
    }
}


#[derive(Debug)]
pub enum TagConversionError<'a> {
    Tag(TagType, &'a str),
    List(TagType, &'a str)
}

impl<'a> std::error::Error for TagConversionError<'a> {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        None
    }
}

impl<'a> Display for TagConversionError<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            TagConversionError::Tag(x, y) => write!(f, "couldn't convert {x:?} to {y}"),
            TagConversionError::List(x, y) => write!(f, "couldn't convert {x:?} to Vec<{y}>")
        }
    }
}

pub trait CompoundTryGetInto<T> where Self: Sized {
    type Error;
    fn try_get_into(self, key: &str) -> Result<Option<T>, Self::Error>;
}

macro_rules! tag_conversion {
    ($tag:ident, &$ty:ty, $str:expr) => {
        impl<'a> TryInto<&'a $ty> for &'a Tag {
            type Error = TagConversionError<'a>;
            fn try_into(self) -> Result<&'a $ty, TagConversionError<'a>> {
                match self {
                    Tag::$tag(x) => Ok(x),
                    _ => Err(TagConversionError::Tag(self.tag_type(), $str))
                }
            }
        }

        impl<'a> TryInto<&'a mut $ty> for &'a mut Tag {
            type Error = TagConversionError<'a>;
            fn try_into(self) -> Result<&'a mut $ty, TagConversionError<'a>> {
                match self {
                    Tag::$tag(x) => Ok(x),
                    _ => Err(TagConversionError::Tag(self.tag_type(), $str))
                }
            }
        }

        impl<'a> CompoundTryGetInto<&'a $ty> for &'a Compound {
            type Error = TagConversionError<'a>;
            fn try_get_into(self, key: &str) -> Result<Option<&'a $ty>, TagConversionError<'a>> {
                match self.get(key) {
                    Some(x) => match x {
                        Tag::$tag(y) => Ok(Some(y)),
                        _ => Err(TagConversionError::Tag(x.tag_type(), $str))
                    },
                    None => Ok(None)
                }
            }
        }

        impl<'a> CompoundTryGetInto<&'a mut $ty> for &'a mut Compound {
            type Error = TagConversionError<'a>;
            fn try_get_into(self, key: &str) -> Result<Option<&'a mut $ty>, TagConversionError<'a>> {
                match self.get_mut(key) {
                    Some(x) => match x {
                        Tag::$tag(y) => Ok(Some(y)),
                        _ => Err(TagConversionError::Tag(x.tag_type(), $str))
                    },
                    None => Ok(None)
                }
            }
        }
        
        impl<'a> TryInto<&'a [$ty]> for &'a Tag {
            type Error = TagConversionError<'a>;
            fn try_into(self) -> Result<&'a [$ty], TagConversionError<'a>> {
                match self {
                    Tag::List(List::$tag(x)) => Ok(x),
                    _ => Err(TagConversionError::Tag(self.tag_type(), "{&[$str]}"))
                }
            }
        }

        impl<'a> TryInto<&'a mut [$ty]> for &'a mut Tag {
            type Error = TagConversionError<'a>;
            fn try_into(self) -> Result<&'a mut [$ty], TagConversionError<'a>> {
                match self {
                    Tag::List(List::$tag(x)) => Ok(x),
                    _ => Err(TagConversionError::Tag(self.tag_type(), "{&[$str]}"))
                }
            }
        }

        impl<'a> CompoundTryGetInto<&'a [$ty]> for &'a Compound {
            type Error = TagConversionError<'a>;
            fn try_get_into(self, key: &str) -> Result<Option<&'a [$ty]>, TagConversionError<'a>> {
                match self.get(key) {
                    Some(x) => match x {
                        Tag::List(List::$tag(y)) => Ok(Some(y)),
                        _ => Err(TagConversionError::Tag(x.tag_type(), $str))
                    },
                    None => Ok(None)
                }
            }
        }

        impl<'a> CompoundTryGetInto<&'a mut [$ty]> for &'a mut Compound {
            type Error = TagConversionError<'a>;
            fn try_get_into(self, key: &str) -> Result<Option<&'a mut [$ty]>, TagConversionError<'a>> {
                match self.get_mut(key) {
                    Some(x) => match x {
                        Tag::List(List::$tag(y)) => Ok(Some(y)),
                        _ => Err(TagConversionError::Tag(x.tag_type(), $str))
                    },
                    None => Ok(None)
                }
            }
        }

        impl<'a> TryInto<&'a [$ty]> for &'a List {
            type Error = TagConversionError<'a>;
            fn try_into(self) -> Result<&'a [$ty], TagConversionError<'a>> {
                match self {
                    List::$tag(x) => Ok(x),
                    _ => Err(TagConversionError::List(self.tag_type(), $str))
                }
            }
        }

        impl<'a> TryInto<&'a mut [$ty]> for &'a mut List {
            type Error = TagConversionError<'a>;
            fn try_into(self) -> Result<&'a mut [$ty], TagConversionError<'a>> {
                match self {
                    List::$tag(x) => Ok(x),
                    _ => Err(TagConversionError::List(self.tag_type(), $str))
                }
            }
        }
    };
    ($tag:ident, $ty:ty, $str:expr) => {
        impl<'a> TryInto<$ty> for &'a Tag {
            type Error = TagConversionError<'a>;
            fn try_into(self) -> Result<$ty, TagConversionError<'a>> {
                match self {
                    Tag::$tag(x) => Ok(*x),
                    _ => Err(TagConversionError::Tag(self.tag_type(), $str))
                }
            }
        }

        impl<'a> CompoundTryGetInto<$ty> for &'a Compound {
            type Error = TagConversionError<'a>;
            fn try_get_into(self, key: &str) -> Result<Option<$ty>, TagConversionError<'a>> {
                match self.get(key) {
                    Some(x) => match x {
                        Tag::$tag(y) => Ok(Some(*y)),
                        _ => Err(TagConversionError::Tag(x.tag_type(), $str))
                    },
                    None => Ok(None)
                }
            }
        }
        tag_conversion!($tag, &$ty, $str);
    }
}

tag_conversion!(Byte, i8, "i8");
tag_conversion!(Short, i16, "i16");
tag_conversion!(Int, i32, "i32");
tag_conversion!(Long, i64, "i64");
tag_conversion!(Float, f32, "f32");
tag_conversion!(Double, f64, "f64");
tag_conversion!(List, &List, "List");
tag_conversion!(Compound, &Compound, "Compound");

impl<'a> TryInto<&'a str> for &'a Tag {
    type Error = TagConversionError<'a>;
    fn try_into(self) -> Result<&'a str, TagConversionError<'a>> {
        match self {
            Tag::String(x) => Ok(x),
            _ => Err(TagConversionError::Tag(self.tag_type(), "str"))
        }
    }
}

impl<'a> TryInto<&'a mut String> for &'a mut Tag {
    type Error = TagConversionError<'a>;
    fn try_into(self) -> Result<&'a mut String, TagConversionError<'a>> {
        match self {
            Tag::String(x) => Ok(x),
            _ => Err(TagConversionError::Tag(self.tag_type(), "str"))
        }
    }
}

impl<'a> TryInto<&'a [String]> for &'a List {
    type Error = TagConversionError<'a>;
    fn try_into(self) -> Result<&'a [String], TagConversionError<'a>> {
        match self {
            List::String(x) => Ok(x),
            _ => Err(TagConversionError::List(self.tag_type(), "str"))
        }
    }
}

impl<'a> TryInto<&'a mut [String]> for &'a mut List {
    type Error = TagConversionError<'a>;
    fn try_into(self) -> Result<&'a mut [String], TagConversionError<'a>> {
        match self {
            List::String(x) => Ok(x),
            _ => Err(TagConversionError::List(self.tag_type(), "str"))
        }
    }
}

impl<'a> CompoundTryGetInto<&'a str> for &'a Compound {
    type Error = TagConversionError<'a>;
    fn try_get_into(self, key: &str) -> Result<Option<&'a str>, TagConversionError<'a>> {
        match self.get(key) {
            Some(x) => match x {
                Tag::String(y) => Ok(Some(y)),
                _ => Err(TagConversionError::Tag(x.tag_type(), "str"))
            },
            None => Ok(None)
        }
    }
}

impl<'a> CompoundTryGetInto<&'a mut String> for &'a mut Compound {
    type Error = TagConversionError<'a>;
    fn try_get_into(self, key: &str) -> Result<Option<&'a mut String>, TagConversionError<'a>> {
        match self.get_mut(key) {
            Some(x) => match x {
                Tag::String(y) => Ok(Some(y)),
                _ => Err(TagConversionError::Tag(x.tag_type(), "str"))
            },
            None => Ok(None)
        }
    }
}

impl Tag {
    pub fn try_get(&self, key: &String) -> Result<Option<&Tag>, TagConversionError> {
        let compound: &Compound = self.try_into()?;
        Ok(compound.get(key))
    }

    pub fn tag_type(&self) -> TagType {
        match self {
            Tag::Byte(_) => TagType::Byte,
            Tag::Short(_) => TagType::Short,
            Tag::Int(_) => TagType::Int,
            Tag::Long(_) => TagType::Long,
            Tag::Float(_) => TagType::Float,
            Tag::Double(_) => TagType::Double,
            Tag::ByteArray(_) => TagType::ByteArray,
            Tag::String(_) => TagType::String,
            Tag::List(_) => TagType::List,
            Tag::Compound(_) => TagType::Compound,
            Tag::IntArray(_) => TagType::IntArray,
            Tag::LongArray(_) => TagType::LongArray
        }
    }
}

impl List {
    pub fn tag_type(&self) -> TagType {
        match self {
            List::Empty => TagType::End,
            List::Byte(_) => TagType::Byte,
            List::Short(_) => TagType::Short,
            List::Int(_) => TagType::Int,
            List::Long(_) => TagType::Long,
            List::Float(_) => TagType::Float,
            List::Double(_) => TagType::Double,
            List::ByteArray(_) => TagType::ByteArray,
            List::String(_) => TagType::String,
            List::List(_) => TagType::List,
            List::Compound(_) => TagType::Compound,
            List::IntArray(_) => TagType::IntArray,
            List::LongArray(_) => TagType::LongArray
        }
    }
}