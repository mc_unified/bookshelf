/*
    MIT License

    Copyright (c) 2022 Valence

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

    https://github.com/valence-rs/serde_nbt
 */

use serde::{de,ser};
use std::{
    fmt::{Display, self}, io, iter::FusedIterator
};

/// Module for reading and writing binary NBT streams, can be used for files and packets.
pub mod bin;

mod tag;
pub use tag::{Tag, Compound, List, TagConversionError, CompoundTryGetInto, byte_array, int_array, long_array};
pub(crate) use tag::{ARRAY_ENUM_NAME, BYTE_ARRAY_NAME, INT_ARRAY_NAME, LONG_ARRAY_NAME};

/// Represents the different NBT Tag Types
#[derive(Clone, PartialEq, Debug, Copy)]
pub enum TagType {
    End,
    Byte,
    Short,
    Int,
    Long,
    Float,
    Double,
    ByteArray,
    String,
    List,
    Compound,
    IntArray,
    LongArray,
}

impl TagType {
    /// Turns a byte into it's corresponding NBT Tag Type
    pub fn from_u8(id: u8) -> Result<TagType> {
        match id {
            0x00 => Ok(TagType::End),
            0x01 => Ok(TagType::Byte),
            0x02 => Ok(TagType::Short),
            0x03 => Ok(TagType::Int),
            0x04 => Ok(TagType::Long),
            0x05 => Ok(TagType::Float),
            0x06 => Ok(TagType::Double),
            0x07 => Ok(TagType::ByteArray),
            0x08 => Ok(TagType::String),
            0x09 => Ok(TagType::List),
            0x0A => Ok(TagType::Compound),
            0x0B => Ok(TagType::IntArray),
            0x0C => Ok(TagType::LongArray),
            _ => Err(Error::new_owned(
                format!("{} is an invalid NBT tag id", id)
            )),
        }
    }
}

impl Display for TagType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self {
            TagType::End => write!(f, "TAG_End"),
            TagType::Byte => write!(f, "TAG_Byte"),
            TagType::Short => write!(f, "TAG_Short"),
            TagType::Int => write!(f, "TAG_Int"),
            TagType::Long => write!(f, "TAG_Long"),
            TagType::Float => write!(f, "TAG_Float"),
            TagType::Double => write!(f, "TAG_Double"),
            TagType::ByteArray => write!(f, "TAG_Byte_Array"),
            TagType::String => write!(f, "TAG_String"),
            TagType::List => write!(f, "TAG_List"),
            TagType::Compound => write!(f, "TAG_Compound"),
            TagType::IntArray => write!(f, "TAG_Int_Array"),
            TagType::LongArray => write!(f, "TAG_Long_Array")
        }
    }
}

pub type Result<T> = std::result::Result<T, Error>;

/// An error for serializing and deserializing NBT,
/// provides a back trace to the field that caused the error.
#[derive(Debug)]
pub struct Error {
    /// Box this to keep the error as small as possible. We don't want to
    /// slow down the common case where no error occurs.
    inner: Box<ErrorInner>,
}

#[derive(Debug)]
struct ErrorInner {
    trace: Vec<String>,
    cause: Cause,
}

#[derive(Debug)]
enum Cause {
    Io(io::Error),
    Mutf8(mutf8::Error),
    // catch-all errors
    Owned(Box<str>),
    Static(&'static str),
}

impl Error {
    pub(crate) fn new_owned(msg: impl Into<Box<str>>) -> Self {
        Self {
            inner: Box::new(ErrorInner {
                trace: Vec::new(),
                cause: Cause::Owned(msg.into()),
            }),
        }
    }

    pub(crate) fn new_static(msg: &'static str) -> Self {
        Self {
            inner: Box::new(ErrorInner {
                trace: Vec::new(),
                cause: Cause::Static(msg),
            }),
        }
    }

    pub(crate) fn field(mut self, ctx: impl Into<String>) -> Self {
        self.inner.trace.push(ctx.into());
        self
    }

    /// Returns an iterator through the nested fields of an NBT compound to the
    /// location where the error occurred.
    ///
    /// The iterator's `Item` is the name of the current field.
    pub fn trace(
        &self,
    ) -> impl DoubleEndedIterator<Item = &str> + ExactSizeIterator + FusedIterator + Clone + '_
    {
        self.inner.trace.iter().rev().map(|s| s.as_str())
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let len = self.inner.trace.len();

        if len > 0 {
            write!(f, "(")?;
            for (i, ctx) in self.trace().enumerate() {
                write!(f, "{ctx}")?;

                if i != len - 1 {
                    write!(f, " → ")?;
                }
            }
            write!(f, ") ")?;
        }

        match &self.inner.cause {
            Cause::Io(e) => e.fmt(f),
            Cause::Mutf8(e) => e.fmt(f),
            Cause::Owned(s) => write!(f, "{s}"),
            Cause::Static(s) => write!(f, "{s}"),
        }
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match &self.inner.cause {
            Cause::Io(e) => Some(e),
            Cause::Mutf8(e) => Some(e),
            Cause::Owned(_) => None,
            Cause::Static(_) => None,
        }
    }
}

impl ser::Error for Error {
    fn custom<T>(msg: T) -> Self
    where
        T: Display,
    {
        Error::new_owned(format!("{msg}"))
    }
}

impl de::Error for Error {
    fn custom<T>(msg: T) -> Self
    where
        T: Display,
    {
        Error::new_owned(format!("{msg}"))
    }
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        Self {
            inner: Box::new(ErrorInner {
                trace: Vec::new(),
                cause: Cause::Io(e),
            }),
        }
    }
}

impl From<mutf8::Error> for Error {
    fn from(e: mutf8::Error) -> Self {
        Self {
            inner: Box::new(ErrorInner {
                trace: Vec::new(),
                cause: Cause::Mutf8(e),
            }),
        }
    }
}