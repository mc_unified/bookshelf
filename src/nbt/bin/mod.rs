pub mod ser;
pub mod de;

use std::{
    io::{Read, Write},
    marker::PhantomData
};

use crate::nbt::Result;
use endermite::Endianness;
use serde::Serialize;

/// Takes in a [`Read`] implementation `R` and deserializes it as binary to a [`serde::Deserialize`] implementation `D`
/// using endianness `E`.
/// 
/// Returns a tuple where the first element is the root name and the second element is the deserialized data.
/// 
/// # Examples
/// ```
/// use crate::{bin::BE, nbt::{Tag, from_reader}};
/// use std::fs::read;
/// 
/// let input = read("bigtest.nbt")?;
/// let (root_name, data) = from_reader::<BE, _, Tag>(&mut &input[..], true)?;
/// ```
pub fn from_reader<'de, E: Endianness, R: Read, D: serde::Deserialize<'de>>(
    reader: R,
    has_root_name: bool,
) -> Result<(Option<String>, D)> {
    let mut deserializer = de::RootDeserializer::<E, R>::from_reader(reader, has_root_name);
    let d = D::deserialize(&mut deserializer)?;
    Ok((deserializer.root_name, d))
}

/// Takes in a [`Write`] implementation `W` and a [`serde::Serialize`] implementation `S` and writes it as binary in
/// endianness `E`.
pub fn to_writer<'a, E: Endianness, W: Write, S: Serialize + ?Sized>(mut writer: W, value: &S, root_name: Option<&'a str>) -> Result<()> {
    Ok(value.serialize(&mut ser::PayloadSerializer {
        writer: &mut writer,
        endianness: PhantomData::<E>,
        state: match root_name {
            Some(name) => ser::State::Named(name),
            None => ser::State::NamelessRoot,
        }
    })?)
}

#[cfg(test)]
mod test {
    use super::*;
    use endermite::BE;
    use crate::nbt::Tag;
    use std::fs::read;

    #[test]
    fn serde() -> Result<()> {
        let input = read("test/bigtest.nbt")?;

        let (root_name, data) = from_reader::<BE, _, Tag>(&input[..], true)?;

        assert_eq!(root_name, Some("Level".to_string()));

        let mut output = vec![0; input.len()];

        to_writer::<BE, _, Tag>(&mut output[..], &data, root_name.as_deref())?;

        Ok(())
    }

    #[test]
    fn invalid_tag_id() -> Result<()> {
        // {a: [B;], b: {<invalid tag id>}}
        let input: [u8; 16] = 
        [0x0A, 0x00, 0x00, 0x07, 0x00, 0x01, 0x41, 0x00, 0x00, 0x00, 0x00, 0x0A, 0x00, 0x01, 0x42, 0x0F];
        let result = from_reader::<BE, _, Tag>(&input[..], true);
        println!("{}", result.unwrap_err());

        Ok(())
    }
}