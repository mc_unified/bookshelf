/*
    MIT License

    Copyright (c) 2022 Valence

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

    https://github.com/valence-rs/serde_nbt
 */

use crate::nbt::{
    BYTE_ARRAY_NAME, INT_ARRAY_NAME, LONG_ARRAY_NAME,
    Error, Result,
    TagType,
};
use endermite::{Endianness, BinRead};
use serde::de::{
    self, value::StrDeserializer, DeserializeSeed, EnumAccess, MapAccess, SeqAccess, VariantAccess,
    Visitor, Error as _, Unexpected
};
use serde::forward_to_deserialize_any;
use mutf8;
use std::{io::Read, marker::PhantomData};

pub struct RootDeserializer<E: Endianness, R: Read> {
    reader: R,
    endianness: PhantomData<E>,
    has_root_name: bool,
    pub root_name: Option<String>,
}

impl<E: Endianness, R: Read> RootDeserializer<E, R> {
    pub fn from_reader(reader: R, has_root_name: bool) -> Self {
        Self {
            reader,
            endianness: PhantomData,
            has_root_name,
            root_name: None,
        }
    }

    fn read_name(&mut self) -> Result<TagType> {
        let tag_type = TagType::from_u8(self.reader.bin_read::<E, _>()?)?;

        if self.has_root_name {
            let len = self.reader.bin_read::<E, u16>()?;
            let bytes = self.reader.bin_read_vec::<E, u8>(len as usize)?; 
            self.root_name = Some(mutf8::decode(&bytes)?.to_string());
        }

        Ok(tag_type)
    }
}

impl<'de, 'a, E: Endianness, R: Read> de::Deserializer<'de> for &'a mut RootDeserializer<E, R> {
    type Error = Error;

    forward_to_deserialize_any! {
        bool i8 i16 i32 i64 i128 u8 u16 u32 u64 u128 f32 f64 char str string
        bytes byte_buf option unit unit_struct newtype_struct seq tuple
        tuple_struct map enum identifier ignored_any
    }

    fn deserialize_any<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        let tag_type = self.read_name()?;

        PayloadDeserializer::<E, _>::from_reader(&mut self.reader, tag_type).deserialize_any(visitor)
    }

    fn deserialize_struct<V>(
        self,
        name: &'static str,
        fields: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        let tag_type = self.read_name()?;

        PayloadDeserializer::<E, _>::from_reader(&mut self.reader, tag_type)
            .deserialize_struct(name, fields, visitor)
    }

    fn is_human_readable(&self) -> bool {
        false
    }
}

struct PayloadDeserializer<'a, E: Endianness, R: Read> {
    reader: &'a mut R,
    endianness: PhantomData<E>,
    tag_type: TagType,
}

impl<'a, E: Endianness, R: Read> PayloadDeserializer<'a, E, R> {
    fn from_reader(reader: &'a mut R, tag_type: TagType) -> Self {
        Self { reader, endianness: PhantomData, tag_type }
    }

    fn read_string(&mut self) -> Result<String> {
        let len = self.reader.bin_read::<E, u16>()?;
        let bytes = self.reader.bin_read_vec::<E, u8>(len as usize)?;
        Ok(mutf8::decode(&bytes)?.to_string())
    }
}

impl<'de: 'a, 'a, E: Endianness, R: Read> de::Deserializer<'de> for PayloadDeserializer<'a, E, R> {
    type Error = Error;

    forward_to_deserialize_any! {
        i8 i16 i32 i64 i128 u8 u16 u32 u64 u128 f32 f64 char str string
        bytes byte_buf unit unit_struct newtype_struct seq tuple
        tuple_struct map enum identifier ignored_any
    }

    fn deserialize_any<V>(mut self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        match self.tag_type {
            TagType::End => unreachable!(),
            TagType::Byte => visitor.visit_i8(self.reader.bin_read::<E, _>()?),
            TagType::Short => visitor.visit_i16(self.reader.bin_read::<E, _>()?),
            TagType::Int => visitor.visit_i32(self.reader.bin_read::<E, _>()?),
            TagType::Long => visitor.visit_i64(self.reader.bin_read::<E, _>()?),
            TagType::Float => visitor.visit_f32(self.reader.bin_read::<E, _>()?),
            TagType::Double => visitor.visit_f64(self.reader.bin_read::<E, _>()?),
            TagType::ByteArray => visitor.visit_enum(Array {
                reader: self.reader,
                endianness: PhantomData::<E>,
                element_type: TagType::Byte,
                variant: BYTE_ARRAY_NAME,
            }),
            TagType::String => visitor.visit_string(self.read_string()?),
            TagType::List => {
                let element_type = TagType::from_u8(self.reader.bin_read::<E, _>()?)?;
                let len = self.reader.bin_read::<E, i32>()?;
                if len < 0 {
                    return Err(Error::new_static(
                        "lists can't have a negative length"
                    ));
                }
                if element_type == TagType::End && len != 0 {
                    return Err(Error::new_static(
                        "Tag_END lists must have length zero"
                    ));
                }
                visitor.visit_seq(List {
                    reader: self.reader,
                    endianness: PhantomData::<E>,
                    element_type,
                    remaining: len as u32,
                })
            }
            TagType::Compound => visitor.visit_map(Compound {
                reader: self.reader,
                endianness: PhantomData::<E>,
                value_type: TagType::End,
                fields: &[],
            }),
            TagType::IntArray => visitor.visit_enum(Array {
                reader: self.reader,
                endianness: PhantomData::<E>,
                element_type: TagType::Int,
                variant: INT_ARRAY_NAME,
            }),
            TagType::LongArray => visitor.visit_enum(Array {
                reader: self.reader,
                endianness: PhantomData::<E>,
                element_type: TagType::Long,
                variant: LONG_ARRAY_NAME,
            }),
        }
    }

    fn deserialize_bool<V: Visitor<'de>>(self, visitor: V) -> Result<V::Value> {
        if self.tag_type == TagType::Byte {
            match self.reader.bin_read::<E, i8>()? {
                0 => visitor.visit_bool(false),
                1 => visitor.visit_bool(true),
                n => visitor.visit_i8(n),
            }
        } else {
            self.deserialize_any(visitor)
        }
    }

    fn deserialize_option<V: Visitor<'de>>(self, visitor: V) -> Result<V::Value> {
        visitor.visit_some(self)
    }

    fn deserialize_struct<V: Visitor<'de>>(
        self,
        _name: &'static str,
        fields: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value> {
        if self.tag_type == TagType::Compound {
            visitor.visit_map(Compound {
                reader: self.reader,
                endianness: PhantomData::<E>,
                value_type: self.tag_type,
                fields,
            })
        } else {
            self.deserialize_any(visitor)
        }
    }

    fn is_human_readable(&self) -> bool {
        false
    }
}

struct List<'a, E: Endianness, R: Read> {
    reader: &'a mut R,
    endianness: PhantomData<E>,
    element_type: TagType,
    remaining: u32,
}

impl<'de: 'a, 'a, E: Endianness, R: Read> SeqAccess<'de> for List<'a, E, R> {
    type Error = Error;

    fn next_element_seed<T: DeserializeSeed<'de>>(&mut self, seed: T) -> Result<Option<T::Value>> {
        if self.remaining > 0 {
            self.remaining -= 1;

            seed.deserialize(PayloadDeserializer {
                reader: self.reader,
                endianness: PhantomData::<E>,
                tag_type: self.element_type,
            })
            .map(Some)
        } else {
            Ok(None)
        }
    }

    fn size_hint(&self) -> Option<usize> {
        Some(self.remaining as usize)
    }
}

struct Compound<'a, E: Endianness, R: Read> {
    reader: &'a mut R,
    endianness: PhantomData<E>,
    value_type: TagType,
    fields: &'static [&'static str],
}

impl<'de: 'a, 'a, E: Endianness, R: Read> MapAccess<'de> for Compound<'a, E, R> {
    type Error = Error;
    fn next_key_seed<K: DeserializeSeed<'de>>(&mut self, seed: K) -> Result<Option<K::Value>> {
        self.value_type = TagType::from_u8(self.reader.bin_read::<E, _>()?)?;
        if self.value_type == TagType::End {
            return Ok(None);
        }
        seed.deserialize(PayloadDeserializer {
            reader: self.reader,
            endianness: PhantomData::<E>,
            tag_type: TagType::String,
        })
        .map(Some)
        .map_err(|e| match self.fields {
            [f, ..] => e.field(*f),
            [] => e,
        })
    }

    fn next_value_seed<V: DeserializeSeed<'de>>(&mut self, seed: V) -> Result<V::Value> {
        let field = match self.fields {
            [field, rest @ ..] => {
                self.fields = rest;
                Some(*field)
            }
            [] => None,
        };
        seed.deserialize(PayloadDeserializer {
            reader: self.reader,
            endianness: PhantomData::<E>,
            tag_type: self.value_type,
        }).map_err(|e| match field {
            Some(f) => e.field(f),
            None => e,
        })
    }
}

struct Array<'a, E: Endianness, R: Read> {
    reader: &'a mut R,
    endianness: PhantomData<E>,
    element_type: TagType,
    variant: &'a str,
}

impl<'de: 'a, 'a, E: Endianness, R: Read> EnumAccess<'de> for Array<'a, E, R> {
    type Error = Error;
    type Variant = Self;

    fn variant_seed<V>(self, seed: V) -> Result<(V::Value, Self::Variant)>
    where
        V: DeserializeSeed<'de>,
    {
        Ok((
            seed.deserialize(StrDeserializer::<Error>::new(self.variant))?,
            self,
        ))
    }
}

impl<'de: 'a, 'a, E: Endianness, R: Read> VariantAccess<'de> for Array<'a, E, R> {
    type Error = Error;

    fn unit_variant(self) -> Result<()> {
        Err(Error::invalid_type(
            Unexpected::NewtypeVariant,
            &"struct variant",
        ))
    }

    fn newtype_variant_seed<T>(self, seed: T) -> Result<T::Value>
    where
        T: DeserializeSeed<'de>,
    {
        seed.deserialize(self)
    }

    fn tuple_variant<V>(self, _len: usize, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        Err(Error::new_static(
            "Tried to parse internal array enum as a tuple variant"
        ))
    }

    fn struct_variant<V>(self, _fields: &'static [&'static str], _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        Err(Error::new_static(
            "Tried to parse internal array enum as a struct variant"
        ))
    }
}

impl<'de: 'a, 'a, E: Endianness, R: Read> de::Deserializer<'de> for Array<'a, E, R> {
    type Error = Error;

    forward_to_deserialize_any! {
        bool i8 i16 i32 i64 i128 u8 u16 u32 u64 u128 f32 f64 char str string
        bytes byte_buf option unit unit_struct newtype_struct seq tuple
        tuple_struct map struct enum identifier ignored_any
    }

    fn deserialize_any<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        let len = self.reader.bin_read::<E, i32>()?;

        if len < 0 {
            return Err(Error::new_static("arrays can't have a negative length"));
        }

        visitor.visit_seq(List {
            reader: self.reader,
            endianness: PhantomData::<E>,
            element_type: self.element_type,
            remaining: len as u32,
        })
    }

    fn is_human_readable(&self) -> bool {
        false
    }
}
