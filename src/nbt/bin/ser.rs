/*
    MIT License

    Copyright (c) 2022 Valence

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

    https://github.com/valence-rs/serde_nbt
 */

use crate::nbt::{
    ARRAY_ENUM_NAME, BYTE_ARRAY_NAME, INT_ARRAY_NAME, LONG_ARRAY_NAME,
    Error, Result,
    TagType
};
use endermite::{Endianness, BinWrite};
use serde::{Serialize, Serializer, ser::{
    Impossible, SerializeSeq, SerializeMap, SerializeStruct, SerializeTuple
}};
use std::{io::Write, marker::PhantomData};

type Imp = Impossible<(), Error>;

pub struct PayloadSerializer<'a, E: Endianness, W: Write> {
    pub writer: &'a mut W,
    pub endianness: PhantomData<E>,
    pub state: State<'a>,
}

pub enum State<'a> {
    Named(&'a str),
    FirstListElement(TagType, i32),
    SeqElement(TagType),
    Array(TagType),
    NamelessRoot,
}

impl<'a, E: Endianness, W: Write> PayloadSerializer<'a, E, W> {
    fn check_state(&mut self, tag: TagType) -> Result<()> {
        match &mut self.state {
            State::Named(name) => {
                self.writer.bin_write::<E, _>(&(tag as u8))?;
                self.writer.bin_write::<E, _>(&(name.len() as u16))?;
                self.writer.bin_write::<E, [u8]>(&mutf8::encode(name))?;
            },
            State::FirstListElement(element_type, len) => {
                self.writer.bin_write::<E, _>(&(tag as u8))?;
                self.writer.bin_write::<E, _>(len)?;
                *element_type = tag;
            },
            State::SeqElement(element_type) => {
                if tag != *element_type {
                    return Err(Error::new_static("can't have heterogeneous lists or arrays"))
                }
            },
            State::Array(element_type) => {
                return Err(Error::new_owned(format!(
                    "expected a seq of {element_type:?}s, got {tag:?} instead"
                )))
            },
            State::NamelessRoot => self.writer.bin_write::<E, _>(&(tag as u8))?
        }
        Ok(())
    }
}

macro_rules! ser {
    ($fn:ident, $dt:ty, $tag:ident) => {
        fn $fn(self, v: $dt) -> Result<Self::Ok> {
            self.check_state(TagType::$tag)?;
            Ok(self.writer.bin_write::<E, _>(&v)?)
        }
    };
}

macro_rules! unsupported_err {
    ($str:literal) => {
        Err(Error::new_static(concat!($str, " is not supported")))
    };
}

macro_rules! unsupported {
    ($fn:ident, $dt:ty, $str:literal) => {
        fn $fn(self, _v: $dt) -> Result<Self::Ok> {
            Err(Error::new_static(concat!($str, " is not supported")))
        }
    };
}

impl<'a, E: Endianness, W: Write> Serializer for &'a mut PayloadSerializer<'_, E, W> {
    type Ok = ();
    type Error = Error;
    type SerializeTupleStruct = Imp;
    type SerializeTupleVariant = Imp;
    type SerializeStructVariant = Imp;
    type SerializeSeq = Seq<'a, E, W>;
    type SerializeTuple = Seq<'a, E, W>;
    type SerializeMap = Map<'a, E, W>;
    type SerializeStruct = Struct<'a, E, W>;

    ser!(serialize_bool, bool, Byte);
    ser!(serialize_i8, i8, Byte);
    ser!(serialize_i16, i16, Short);
    ser!(serialize_i32, i32, Int);
    ser!(serialize_i64, i64, Long);
    ser!(serialize_f32, f32, Float);
    ser!(serialize_f64, f64, Double);

    fn serialize_none(self) -> Result<Self::Ok> {
        Ok(())
    }

    fn serialize_some<T: Serialize + ?Sized>(self, v: &T) -> Result<Self::Ok> {
        v.serialize(self)
    }

    fn serialize_str(self, v: &str) -> Result<Self::Ok> {
        self.check_state(TagType::String)?;
        self.writer.bin_write::<E, _>(&(v.len() as u16))?;
        self.writer.bin_write::<E, [u8]>(&mutf8::encode(v))?;
        Ok(())
    }

    fn serialize_seq(self, len: Option<usize>) -> Result<Self::SerializeSeq> {
        if let State::Array(element_type) = self.state {
            let len = match len {
                Some(len) => len,
                None => return Err(Error::new_static("array length must be known up front")),
            };

            match len.try_into() {
                Ok(len) => {
                    self.writer.bin_write::<E, i32>(&len)?;
                    Ok(Seq {
                        writer: self.writer,
                        endianness: PhantomData::<E>,
                        element_type,
                        remaining: len,
                    })
                }
                Err(_) => Err(Error::new_static("length of array exceeds i32::MAX")),
            }
        } else {
            self.check_state(TagType::List)?;

            let len = match len {
                Some(len) => len,
                None => return Err(Error::new_static("list length must be known up front")),
            };

            match len.try_into() {
                Ok(len) => Ok(Seq {
                    writer: self.writer,
                    endianness: PhantomData::<E>,
                    element_type: TagType::End,
                    remaining: len,
                }),
                Err(_) => Err(Error::new_static("length of list exceeds i32::MAX")),
            }
        }
    }

    
    fn serialize_tuple(self, len: usize) -> Result<Self::SerializeTuple> {
        if let State::Array(element_type) = self.state {
            match len.try_into() {
                Ok(len) => {
                    self.writer.bin_write::<E, i32>(&len)?;
                    Ok(Seq {
                        writer: self.writer,
                        endianness: PhantomData::<E>,
                        element_type,
                        remaining: len,
                    })
                }
                Err(_) => Err(Error::new_static("length of array exceeds i32::MAX")),
            }
        } else {
            self.check_state(TagType::List)?;

            match len.try_into() {
                Ok(len) => Ok(Seq {
                    writer: self.writer,
                    endianness: PhantomData::<E>,
                    element_type: TagType::End,
                    remaining: len,
                }),
                Err(_) => Err(Error::new_static("length of list exceeds i32::MAX")),
            }
        }
    }

    fn serialize_map(self, _len: Option<usize>) -> Result<Self::SerializeMap> {
        self.check_state(TagType::Compound)?;

        Ok(Map {
            writer: self.writer,
            endianness: PhantomData::<E>
        })
    }

    fn serialize_struct(self, _name: &'static str, _len: usize) -> Result<Self::SerializeStruct> {
        self.check_state(TagType::Compound)?;

        Ok(Struct {
            writer: self.writer,
            endianness: PhantomData::<E>
        })
    }

    fn serialize_newtype_variant<T: Serialize + ?Sized>(
            self,
            name: &'static str,
            _variant_index: u32,
            variant: &'static str,
            value: &T,
        ) -> Result<Self::Ok> {
        let (array_type, element_type) = match (name, variant) {
            (ARRAY_ENUM_NAME, BYTE_ARRAY_NAME) => {
                (TagType::ByteArray, TagType::Byte)
            },
            (ARRAY_ENUM_NAME, INT_ARRAY_NAME) => {
                (TagType::IntArray, TagType::Int)
            },
            (ARRAY_ENUM_NAME, LONG_ARRAY_NAME) => {
                (TagType::LongArray, TagType::Long)
            },
            _ => return unsupported_err!("newtype variant")
        };
        self.check_state(array_type)?;
        value.serialize(&mut PayloadSerializer {
            writer: self.writer,
            endianness: PhantomData::<E>,
            state: State::Array(element_type)
        })
    }

    unsupported!(serialize_u8, u8, "u8");
    unsupported!(serialize_u16, u16, "u16");
    unsupported!(serialize_u32, u32, "u32");
    unsupported!(serialize_u64, u64, "u64");
    unsupported!(serialize_u128, u128, "u128");
    unsupported!(serialize_i128, i128, "i128");
    unsupported!(serialize_char, char, "char");
    unsupported!(serialize_bytes, &[u8], "&[u8]");
    unsupported!(serialize_unit_struct, &'static str, "unit struct");

    fn serialize_unit(self) -> Result<Self::Ok> {
        unsupported_err!("()")
    }

    fn serialize_newtype_struct<T: Serialize + ?Sized>(
            self,
            _name: &'static str,
            _value: &T,
        ) -> Result<Self::Ok> {
        unsupported_err!("newtype struct")
    }

    fn serialize_struct_variant(
            self,
            _name: &'static str,
            _variant_index: u32,
            _variant: &'static str,
            _len: usize,
        ) -> Result<Self::SerializeStructVariant> {
        unsupported_err!("struct variant")
    }

    fn serialize_tuple_struct(
            self,
            _name: &'static str,
            _len: usize,
        ) -> Result<Self::SerializeTupleStruct> {
        unsupported_err!("tuple struct")
    }

    fn serialize_tuple_variant(
            self,
            _name: &'static str,
            _variant_index: u32,
            _variant: &'static str,
            _len: usize,
        ) -> Result<Self::SerializeTupleVariant> {
            unsupported_err!("tuple variant")
    }

    fn serialize_unit_variant(
            self,
            _name: &'static str,
            _variant_index: u32,
            _variant: &'static str,
        ) -> Result<Self::Ok> {
            unsupported_err!("unit variant")
    }

    fn is_human_readable(&self) -> bool {
        false
    }
}

pub struct Seq<'a, E: Endianness, W: Write> {
    writer: &'a mut W,
    endianness: PhantomData<E>,
    element_type: TagType,
    remaining: i32,
}
macro_rules! seq {
    ($trait:ident) => {
        impl<E: Endianness, W: Write> $trait for Seq<'_, E, W> {
            type Ok = ();
            type Error = Error;
        
            fn serialize_element<T: Serialize + ?Sized>(&mut self, value: &T) -> Result<()> {
                if self.remaining <= 0 {
                    return Err(Error::new_static("attempted to serialize more elements than specified"));
                }
        
                if self.element_type == TagType::End {
                    let mut ser = PayloadSerializer{
                        writer: self.writer,
                        endianness: PhantomData::<E>,
                        state: State::FirstListElement(TagType::End, self.remaining)
                    };
                    value.serialize(&mut ser)?;
                    if let State::FirstListElement(element_type, _) = ser.state {
                        self.element_type = element_type;
                    }
                } else {
                    value.serialize(&mut PayloadSerializer {
                        writer: self.writer,
                        endianness: PhantomData::<E>,
                        state: State::SeqElement(self.element_type)
                    })?;
                }
        
                self.remaining -= 1;
                Ok(())
            }
        
            fn end(self) -> Result<Self::Ok> {
                // didn't write any tags, write an empty list payload
                if self.element_type == TagType::End {
                    self.writer.bin_write::<E, _>(&0u8)?;
                    self.writer.bin_write::<E, _>(&0i32)?;
                }
        
                Ok(())
            }
        }
    };
}

seq!(SerializeSeq);
seq!(SerializeTuple);

pub struct Map<'a, E: Endianness, W: Write> {
    writer: &'a mut W,
    endianness: PhantomData<E>
}

impl<E: Endianness, W: Write> SerializeMap for Map<'_, E, W> {
    type Ok = ();
    type Error = Error;

    fn serialize_key<T: Serialize + ?Sized>(&mut self, _key: &T) -> Result<()> {
        Err(Error::new_static(
            "map keys cannot be serialized individually"
        ))
    }

    fn serialize_value<T: Serialize + ?Sized>(&mut self, _key: &T) -> Result<()> {
        Err(Error::new_static(
            "map values cannot be serialized individually"
        ))
    }

    fn serialize_entry<K: Serialize + ?Sized, V: Serialize + ?Sized>(
            &mut self,
            key: &K,
            value: &V,
        ) -> Result<()>{
        key.serialize(MapEntry {
            writer: self.writer,
            endianness: PhantomData::<E>,
            value,
        })
    }

    fn end(self) -> Result<Self::Ok> {
        Ok(self.writer.bin_write::<E, _>(&0u8)?)
    }
}

pub struct MapEntry<'a, E: Endianness, W: Write, V: Serialize + ?Sized> {
    writer: &'a mut W,
    endianness: PhantomData<E>,
    value: &'a V,
}

macro_rules! unsupported_key {
    () => {
        Err(Error::new_static("only strings can be keys"))
    };

    ($fn:ident, $dt:ty) => {
        fn $fn(self, _v: $dt) -> Result<Self::Ok> {
            Err(Error::new_static("only strings can be keys"))
        }
    };
}

impl<E: Endianness, W: Write, V: Serialize + ?Sized> Serializer for MapEntry<'_, E, W, V> {
    type Ok = ();
    type Error = Error;
    type SerializeTuple = Imp;
    type SerializeTupleStruct = Imp;
    type SerializeTupleVariant = Imp;
    type SerializeStructVariant = Imp;
    type SerializeSeq = Imp;
    type SerializeMap = Imp;
    type SerializeStruct = Imp;

    fn serialize_str(self, v: &str) -> Result<Self::Ok> {
        self.value.serialize(&mut PayloadSerializer{
            writer: self.writer,
            endianness: PhantomData::<E>,
            state: State::Named(v),
        })
    }
    
    unsupported_key!(serialize_bool, bool);
    unsupported_key!(serialize_i8, i8);
    unsupported_key!(serialize_i16, i16);
    unsupported_key!(serialize_i32, i32);
    unsupported_key!(serialize_i64, i64);
    unsupported_key!(serialize_i128, i128);
    unsupported_key!(serialize_u8, u8);
    unsupported_key!(serialize_u16, u16);
    unsupported_key!(serialize_u32, u32);
    unsupported_key!(serialize_u64, u64);
    unsupported_key!(serialize_u128, u128);
    unsupported_key!(serialize_f32, f32);
    unsupported_key!(serialize_f64, f64);
    unsupported_key!(serialize_char, char);
    unsupported_key!(serialize_bytes, &[u8]);
    unsupported_key!(serialize_unit_struct, &'static str);

    fn serialize_unit_variant(
            self,
            _name: &'static str,
            _variant_index: u32,
            _variant: &'static str,
        ) -> Result<Self::Ok> {
            unsupported_key!()
    }

    fn serialize_none(self) -> Result<Self::Ok> {
        unsupported_key!()
    }

    fn serialize_some<T: Serialize + ?Sized>(self, _value: &T) -> Result<Self::Ok> {
        unsupported_key!()
    }

    fn serialize_unit(self) -> Result<Self::Ok> {
        unsupported_key!()
    }

    fn serialize_newtype_struct<T: Serialize + ?Sized>(
            self,
            _name: &'static str,
            _value: &T,
        ) -> Result<Self::Ok> {
            unsupported_key!()
    }

    fn serialize_newtype_variant<T: Serialize + ?Sized>(
            self,
            _name: &'static str,
            _variant_index: u32,
            _variant: &'static str,
            _value: &T,
        ) -> Result<Self::Ok> {
            unsupported_key!()
    }

    fn serialize_seq(self, _len: Option<usize>) -> Result<Self::SerializeSeq> {
        unsupported_key!()
    }

    fn serialize_tuple(self, _len: usize) -> Result<Self::SerializeTuple> {
        unsupported_key!()
    }

    fn serialize_tuple_struct(
            self,
            _name: &'static str,
            _len: usize,
        ) -> Result<Self::SerializeTupleStruct> {
            unsupported_key!()
    }

    fn serialize_tuple_variant(
            self,
            _name: &'static str,
            _variant_index: u32,
            _variant: &'static str,
            _len: usize,
        ) -> Result<Self::SerializeTupleVariant> {
            unsupported_key!()
    }

    fn serialize_map(self, _len: Option<usize>) -> Result<Self::SerializeMap> {
        unsupported_key!()
    }

    fn serialize_struct(
            self,
            _name: &'static str,
            _len: usize,
        ) -> Result<Self::SerializeStruct> {
        unsupported_key!()
    }

    fn serialize_struct_variant(
            self,
            _name: &'static str,
            _variant_index: u32,
            _variant: &'static str,
            _len: usize,
        ) -> Result<Self::SerializeStructVariant> {
            unsupported_key!()
    }

    fn is_human_readable(&self) -> bool {
        false
    }
}

pub struct Struct<'a, E: Endianness, W: Write> {
    writer: &'a mut W,
    endianness: PhantomData<E>
}

impl<E: Endianness, W: Write> SerializeStruct for Struct<'_, E, W> {
    type Ok = ();
    type Error = Error;

    fn serialize_field<T: Serialize + ?Sized>(
            &mut self,
            key: &'static str,
            value: &T,
        ) -> Result<()> {
        value.serialize(&mut PayloadSerializer {
            writer: self.writer,
            endianness: PhantomData::<E>,
            state: State::Named(key),
        })
    }

    fn end(self) -> std::prelude::v1::Result<Self::Ok, Self::Error> {
        Ok(self.writer.bin_write::<E, u8>(&0)?)
    }
}
