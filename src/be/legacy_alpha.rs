use std::io::{Read, Write, Result};

use crate::{BinaryFile, BE, LE, bin::{BinReader, BinWriter}};

#[derive(Debug)]
pub struct Level {
    /// The level's seed for generation
    pub seed: i32,

    /// The x coordinate of the world spawn
    pub spawn_x: i32,
    /// The y coordinate of the world spawn
    pub spawn_y: i32,
    /// The z coordinate of the world spawn
    pub spawn_z: i32,

    /// Estimated world size in bits (not a verified field)
    pub world_size: u64,

    /// 32bit unix timestamp of when the world was last played
    pub last_played: i32,

    /// The name of the world, uses CP437 characters
    pub world_name: Vec<u8>,
}

impl BinaryFile for Level {
    fn from_reader<R: Read>(reader: R) -> Result<Self> {
        let mut bin_reader = BinReader::<BE, R>::new(reader);
        // skip header
        bin_reader.read::<u64>()?;
        let seed = bin_reader.read()?;
        let spawn_x = bin_reader.read()?;
        let spawn_y = bin_reader.read()?;
        let spawn_z = bin_reader.read()?;
        let world_size = bin_reader.read()?;
        let last_played = bin_reader.read()?;
        let world_name_length = bin_reader.read::<u16>()?;
        let mut world_name = vec![0u8; world_name_length as usize];
        bin_reader.read_slice(&mut world_name)?;
        Ok(Self {
            seed,
            spawn_x,
            spawn_y,
            spawn_z,
            world_size,
            last_played,
            world_name
        })
    }

    fn to_writer<W: Write>(&self, writer: W) -> Result<()> {
        let world_name_length = self.world_name.len();

        let mut bin_writer = BinWriter::<BE, W>::new(writer);
        bin_writer.write(&0x01_00_00_00u32)?;
        let file_length = 30 + world_name_length as u32;
        bin_writer.write_slice(&file_length.to_le_bytes())?;
        bin_writer.write(&self.seed)?;
        bin_writer.write(&self.spawn_x)?;
        bin_writer.write(&self.spawn_y)?;
        bin_writer.write(&self.spawn_z)?;
        bin_writer.write(&self.world_size)?;
        bin_writer.write(&self.last_played)?;
        bin_writer.write(&(world_name_length as u16))?;
        bin_writer.write_slice(&self.world_name)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use std::fs::read;

    #[test]
    fn binary_file() -> Result<()> {
        let input = read("test/be_legacy_alpha_level.dat")?;
        let data = Level::from_reader(&mut &input[..])?;
        let mut output = Vec::<u8>::new();
        data.to_writer(&mut output)?;
        assert_eq!(input, output);
        Ok(())
    }
}