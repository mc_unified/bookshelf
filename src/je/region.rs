use std::fs;
use std::io::{Read, Seek, SeekFrom, Write, self};
use std::fmt::Display;
use std::mem::transmute;
use crate::{nbt::{bin::{from_reader, to_writer}, self}, BE};

/// The size of a sector in bytes
pub const SECTOR_SIZE: u64 = 4096;
/// Bit mask to check that a number of bytes fits evenly in `n` sectors
pub const SECTOR_SIZE_MASK: u64 = 0b1111_1111_1111;
/// The amount to bitshift by to change between bytes and sectors
pub const SECTOR_BIT_SHIFT: u64 = 12;
/// The amount of entries in both header tables
pub const TABLE_SIZE: u16 = 1024;
/// The max chunk size that can be written in bytes
pub const MAX_CHUNK_SIZE: usize = 1049000;

/// An enum representing the different kinds of compression
/// a region file may have.
#[repr(u8)]
pub enum Compression {
    GZip = 1,
    Zlib,
    Uncompressed,
    LZ4,
}

impl Compression {
    fn from_u8(value: u8) -> Result<Self> {
        match value {
            1 => Ok(Compression::GZip),
            2 => Ok(Compression::Zlib),
            3 => Ok(Compression::Uncompressed),
            4 => Ok(Compression::LZ4),
            v => Err(Error::UnknownCompression(v))
        }
    }
}

/// Makes the [`File`](fs::File) a multiple of 4KiB
/// [as per specification](https://minecraft.wiki/w/Region_file_format#Payload).
/// 
/// Returns the new file size in bytes.
#[macro_export]
macro_rules! normalize_size {
    ($file:expr) => {
        {
            let mut size = $file.metadata()?.len();
            let extra = size & SECTOR_SIZE_MASK;
            if extra != 0 {
                size += SECTOR_SIZE - extra;
                $file.set_len(size)?;
            }
            size
        }
    };
}

/// Gets the entry id in the 
/// [header tables](https://minecraft.wiki/w/Region_file_format#Header) for the chunk at (x, z).
#[macro_export]
macro_rules! chunk_id {
    ($x:expr, $z:expr) => {
        (($x & 0b11111) + ($z & 0b11111) * 32) as u16
    };
}

/// Breaks a location value into it's offset and size byte
#[macro_export]
macro_rules! location_parts {
    ($loc:expr) => {
        (($loc & 0xFFFFFF00) >> 8, $loc & 0xFF)
    };
}

/// Moves the cursor of the file to a region sector
#[macro_export]
macro_rules! seek_sector {
    ($file:expr, $offset:expr) => {
        $file.seek(SeekFrom::Start(($offset as u64) << SECTOR_BIT_SHIFT))?;
    };
}

/// An object for handling [region files](https://minecraft.wiki/w/Region_file_format) with any NBT content, 
/// works with both `.mcr` and `.mca` files.
///
/// Allows for random access reading to any chunk in the file using [`serde::Deserialize`] implementations
/// and writing with [`serde::Serialize`] implementations.
/// 
/// # Examples
/// Copying the entities from one chunk to another:
/// ```
/// # use super::*;
/// # use crate::nbt::{error::Result, self};
/// # use serde_derive::{Deserialize, Serialize};
/// # use std::fs::OpenOptions;
/// use flate2::{read::ZlibDecoder, write::ZlibEncoder};
/// 
/// # fn region_doc_test() -> Result {
/// let file = OpenOptions::new()
///    .read(true)
///    .write(true)
///    .open("test/r.0.0.mca")?;
/// let mut region = RegionFile::new(file)?;
///
/// struct CompHandler {}
/// impl CompressionHandler for CompHandler {
///   fn decompress<'r, R: Read + 'r>(read: R, c: Compression) -> Box<dyn Read + 'r> {
///        match c {
///            Compression::Zlib => Box::new(ZlibDecoder::new(read)),
///            _ => panic!("Should of been zlib compressed")
///        }
///    }
/// 
///    fn compress<'w, W: Write + 'w>(write: W) -> (Box<dyn Write + 'w>, Compression) {
///        (
///            Box::new(ZlibEncoder::new(write, flate2::Compression::default())),
///            Compression::Zlib
///        )
///    }
/// }
/// # Ok(()) }
/// #[derive(Serialize, Deserialize, Debug, Clone)]
/// #[serde(rename_all="PascalCase")]
/// #[serde(deny_unknown_fields)]
/// struct EntityChunk {
///    data_version: i32,
///    #[serde(with="nbt::int_array")]
///    position: [i32; 2],
///    entities: Vec<nbt::Compound>
/// }
/// match region.read_chunk::<EntityChunk, CompHandler>(0, 0)? {
///    Some(chunk) => {
///        let mut new_chunk = chunk.clone();
///        new_chunk.position = [1, 0];
///        let mut i = 0;
///        for entity in &mut new_chunk.entities {
///            match entity.get_mut("Pos") {
///                Some(tag) => {
///                    match tag {
///                        nbt::Tag::List(nbt::List::Double(pos)) => {
///                            pos[0] += 16.0;
///                        }
///                        _ => {}
///                    }
///                },
///                None => {}
///            }
///
///            match entity.get_mut("UUID") {
///                Some(uuid) => {
///                    *uuid = nbt::Tag::IntArray(vec![0, 0, 0, i]);
///                },
///                None => {}
///            }
///            i += 1;
///        }
///
///        region.write_chunk::<EntityChunk, CompHandler>(1, 0, &new_chunk, "")?;
///    },
///    None => println!("No chunk data")
/// }
/// ```
pub struct File {
    file: fs::File,
    sector_free: Vec<bool>,
    locations: [u32; TABLE_SIZE as usize],
    chunks: Vec<u16>,
}

impl File {
    /// Creates a new region [`File`] object from rust's [`std::fs::File`] object
    /// 
    /// Makes the underlying rust file a multiple of 4KiB 
    /// [as per specification](https://minecraft.wiki/w/Region_file_format#Payload)
    pub fn new(mut file: fs::File) -> Result<Self> {
        let size = normalize_size!(file) >> SECTOR_BIT_SHIFT;
        if size == 0 {
            return Ok(Self {
                file,
                sector_free: vec![false; 2],
                locations: [0u32; TABLE_SIZE as usize],
                chunks: Vec::new(),
            })
        }
        let mut sector_free = vec![true; size as usize];
        /* safe guard the region header */
            sector_free[0] = false;
            sector_free[1] = false;
        file.seek(SeekFrom::Start(0))?;
        let mut location_bytes = [0u8; SECTOR_SIZE as usize];
        file.read_exact(&mut location_bytes)?;
        if cfg!(target_endian = "little") {
            for chunk in location_bytes.chunks_mut(4) {
                chunk.reverse();
            }
        }
        let locations = unsafe {
            transmute::<_, [u32; TABLE_SIZE as usize]>(location_bytes)
        };
        let mut chunks = Vec::new();
        for i in 0..TABLE_SIZE {
            let loc = locations[i as usize];
            if loc == 0 {continue}
            chunks.push(i);
            let (start, size) = location_parts!(loc);
            let end = start + size as u32;
            if end as usize > sector_free.len() {
                return Err(Error::InvalidChunkLocation("chunk data extends past end of file".into()))
            }
            for sector in start..end {
                sector_free[sector as usize] = false;
            }
        }
        Ok(Self {
            file,
            sector_free,
            locations,
            chunks
        })
    }

    /// Finds an offset in the file with the required free sector space.
    fn find_space(&self, size: u8) -> Option<u32> {
        match self.sector_free.iter().position(|&b| b) {
            Some(free ) => {
                let mut run_start = free;
                let mut run = 0;
                for i in run_start as usize..self.sector_free.len() {
                    if run != 0 {
                        if self.sector_free[i] {run += 1;}
                        else {run = 0}
                    } else if self.sector_free[i] {
                        run_start = i;
                        run = 1;
                    }
                    if run >= size {
                        return Some(run_start as u32)
                    }
                }
                None
            }
            None => None
        }
    }

    fn alloc_chunk(&mut self, chunk: u16, size: u8) -> Result<u32> {
        let offset = match self.find_space(size) {
            None => {
                let offset = self.sector_free.len() as u32;
                self.locations[chunk as usize] = (offset << 8) | size as u32;
                for _ in 0..size {
                    self.sector_free.push(false);
                }
                offset
            },
            Some(offset) => {
                self.locations[chunk as usize] = (offset << 8) | size as u32;
                for i in offset .. offset + size as u32 {
                    self.sector_free[i as usize] = false;
                }
                offset
            }
        };
        Ok(offset)
    }

    /// Deallocates the chunk with id `chunk`
    /// 
    /// This method only frees up the space in the file taken up by the chunk to be used
    /// by future chunk writes that fit in the space, it doesn't remove the data.
    pub fn dealloc_chunk(&mut self, chunk: u16) -> Result<()> {
        let loc = self.locations[chunk as usize];
        if loc == 0 {return Ok(())}
        let (offset, size) = location_parts!(loc);
        let end = offset + size;
        if end as usize > self.sector_free.len() {
            return Err(Error::InvalidChunkLocation("chunk data extends past end of region file".into()))
        }
        self.locations[chunk as usize] = 0;
        for i in offset..end {
            self.sector_free[i as usize] = true;
        }
        let i = self.chunks.iter().position(|&x| x == chunk).unwrap();
        self.chunks.remove(i);
        Ok(())
    }

    /// Attempts to read the chunk with table id `chunk` as `T`, `C`
    /// turns the compressed data from the file and turns it into decompressed
    /// data depending on the compression type passed in.
    /// 
    /// If there is no data for the chunk it returns [`None`],
    /// if it fails to deserialize into `T` it returns an [`Err`].
    pub fn read_chunk<'de, T: serde::Deserialize<'de>, C: CompressionHandler>(
        &mut self, chunk: u16
    ) -> Result<Option<T>> {
        let loc = self.locations[chunk as usize];
        if loc == 0 {return Ok(None)}
        let (offset, size) = location_parts!(loc);
        seek_sector!(self.file, offset);
        let mut buf = vec![0u8; (size << SECTOR_BIT_SHIFT) as usize];
        self.file.read_exact(&mut buf)?;
        let c = Compression::from_u8(buf[4])?;
        let reader = C::decompress(&buf[5..], c);
        Ok(Some(from_reader::<BE, _, T>(reader, true)?.1))
    }

    /// Writes the chunk data `v` to the chunk with id `chunk` with the root name `name`, `C`
    /// turns the uncompressed data into compressed data before writing it to the file.
    pub fn write_chunk<T: serde::Serialize, C: CompressionHandler>(
        &mut self,
        chunk: u16,
        v: &T,
        name: &'static str
    ) -> Result<()> {
        let mut bytes = vec![0u8; 0];
        let (mut writer, c) = C::compress(&mut bytes);
        to_writer::<BE, _, T>(&mut writer, v, Some(name))?;
        drop(writer);
        //add byte for compression marker
        let size = bytes.len() + 1;

        if size > MAX_CHUNK_SIZE {
            return Err(Error::ChunkExceedsMaxSize(size))
        }

        let extra = size as u64 & SECTOR_SIZE_MASK;
        let corrected_size = size as u64 + (SECTOR_SIZE - extra);
        let required_sectors = corrected_size >> SECTOR_BIT_SHIFT;
        let loc = self.locations[chunk as usize];
        let offset = if loc == 0 {
            self.chunks.push(chunk);
            self.alloc_chunk(chunk, required_sectors as u8)?
        }
        else {
            let (old_offset, old_size) = location_parts!(loc);
            if old_size == required_sectors as u32 {
                old_offset
            } else {
                self.alloc_chunk(chunk, required_sectors as u8)?
            }
        };

        seek_sector!(self.file, offset);
        self.file.write(&(size as u32).to_be_bytes())?;
        self.file.write(&[c as u8])?;
        self.file.write(&bytes)?;

        Ok(())
    }

    /// Makes sure any internal state of the region file is written
    pub fn flush(&mut self) -> io::Result<()> {
        let mut location_bytes = unsafe {
            transmute::<_, [u8; SECTOR_SIZE as usize]>(self.locations)
        };
        if cfg!(target_endian = "little") {
            for chunk in location_bytes.chunks_mut(4) {
                chunk.reverse();
            }
        }
        self.file.seek(SeekFrom::Start(0))?;
        self.file.write_all(&location_bytes)
    }

    /// Returns a slice of chunk ids that have chunk data
    pub fn chunks(&self) -> &[u16] {
        &self.chunks
    }

    /// Runs [`File::sync_all`] on the underlying file
    /// 
    /// Can be used to block code execution until all io
    /// operations on the file are finished
    pub fn sync_all(&mut self) -> io::Result<()> {
        self.file.sync_all()
    }

    /// Ensures that the file is a multiple of 4KiB
    /// [as per specification](https://minecraft.wiki/w/Region_file_format#Payload).
    /// 
    /// Returns the new file size in bytes.
    pub fn normalize_size(&mut self) -> io::Result<u64> {
        Ok(normalize_size!(self.file))
    }
}

///Trait for converting readers and writers to their compressed or decompressed counterparts
/// 
/// [`CompressionHandler::decompress`] takes in a compressed byte stream and a [`Compression`] variant
/// representing the type of compression on the stream, returns an uncompressed byte stream.
/// 
/// [`CompressionHandler::compress`] takes in a empty compressed byte steam and returns
/// a uncompressed byte stream and [`Compression`] variant representing the compression type.
pub trait CompressionHandler {
    fn decompress<'r, R: Read + 'r>(read: R, c: Compression) -> Box<dyn Read + 'r>;
    fn compress<'w, W: Write + 'w>(write: W) -> (Box<dyn Write + 'w>, Compression);
}

pub type Result<T> = std::result::Result<T, Error>;


/// An error for region file IO operations
#[derive(Debug)]
pub enum Error {
    UnknownCompression(u8),
    ChunkExceedsMaxSize(usize),
    InvalidChunkLocation(Box<str>),
    NBT(nbt::Error),
    Io(io::Error),
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::UnknownCompression(x) => write!(f, "unknown compression type: {x}"),
            Error::ChunkExceedsMaxSize(x) => write!(f, "{x} exceeds the max chunk size of {MAX_CHUNK_SIZE}"),
            Error::InvalidChunkLocation(reason) => write!(f, "invalid chunk location: {reason}"),
            Error::NBT(e) => e.fmt(f),
            Error::Io(e) => e.fmt(f),
        }
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Error::UnknownCompression(_) => None,
            Error::InvalidChunkLocation(_) => None,
            Error::ChunkExceedsMaxSize(_) => None,
            Error::NBT(e) => Some(e),
            Error::Io(e) => Some(e),
        }
    }
}

impl From<nbt::Error> for Error {
    fn from(value: nbt::Error) -> Self {
        Error::NBT(value)
    }
}

impl From<io::Error> for Error {
    fn from(value: io::Error) -> Self {
        Error::Io(value)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::nbt;
    use flate2::{read::ZlibDecoder, write::ZlibEncoder};
    use serde_derive::{Deserialize, Serialize};
    use std::fs::OpenOptions;

    #[test]
    fn table_entry_id() {
        assert_eq!(chunk_id!(0, 0), 0);
        assert_eq!(chunk_id!(1, 0), 1);
        assert_eq!(chunk_id!(2, 0), 2);
        assert_eq!(chunk_id!(0, 1), 32);
        assert_eq!(chunk_id!(0, 2), 64);
        assert_eq!(chunk_id!(1, 1), 33);

        assert_eq!(chunk_id!(32, 32), 0);
        assert_eq!(chunk_id!(32, 33), 32);
        assert_eq!(chunk_id!(33, 33), 33);
    }

    #[test]
    fn read_chunk() -> Result<()> {
        let file = OpenOptions::new()
            .read(true)
            .write(true)
            .open("test/entities.mca")?;
        let mut region = File::new(file)?;

        struct CompHandler {}
        impl CompressionHandler for CompHandler {
            fn decompress<'r, R: Read + 'r>(read: R, c: Compression) -> Box<dyn Read + 'r> {
                match c {
                    Compression::Zlib => Box::new(ZlibDecoder::new(read)),
                    _ => panic!("Should of been zlib compressed")
                }
            }

            fn compress<'w, W: Write + 'w>(write: W) -> (Box<dyn Write + 'w>, Compression) {
                (
                    Box::new(ZlibEncoder::new(write, flate2::Compression::default())),
                    Compression::Zlib
                )
            }
        }

        match region.read_chunk::<nbt::Compound, CompHandler>(0)? {
            Some(nbt_data) => {println!("{:?}", nbt_data)}
            None => panic!("Chunk location was 0")
        }
        Ok(())
    }
}