//! A library for reading and writing various minecraft files from
//! many different editions and versions of the game.
#![cfg_attr(docsrs, feature(doc_cfg))]

use std::fs::File;
use std::io::{Read, Result, Write};

/// Module for serializing and deserializing NBT data
#[cfg(feature = "nbt")]
#[cfg_attr(docsrs, doc(cfg(feature = "nbt")))]
pub mod nbt;

/// Module for writing and reading `Minecraft: Java Edition` files
#[cfg(feature = "je")]
#[cfg_attr(docsrs, doc(cfg(feature = "je")))]
pub mod je;

/// Module for writing and reading `Minecraft: Bedrock Edition` files
#[cfg(feature = "be")]
#[cfg_attr(docsrs, doc(cfg(feature = "be")))]
pub mod be;

/// Trait for any struct that can be converted to and from a binary stream.
pub trait BinaryFile {
    /// Reads a binary stream and returns an instance of `Self`
    fn from_reader<R: Read>(reader: R) -> Result<Self> where Self: Sized;
    /// Writes the struct as a binary stream
    fn to_writer<W: Write>(&self, writer: W) -> Result<()>;
}

/// Trait for structs that interact with random access files
/// such as region files.
pub trait RandomAccess {
    fn new(file: File) -> Result<Self> where Self: Sized;
}